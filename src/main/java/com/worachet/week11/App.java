package com.worachet.week11;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        Bat bat1 = new Bat("Batman");
        bat1.eat();
        bat1.sleep();
        bat1.takeoff();
        bat1.fly();
        bat1.landing();

        Fish fish1 = new Fish("Nemo");
        fish1.eat();
        fish1.sleep();
        fish1.swim();

        Plane plane1 = new Plane("Boing", "Boing Engine");
        plane1.takeoff();
        plane1.fly();
        plane1.landing();

        Crocodile crocodile1 = new Crocodile("Red");
        crocodile1.craw();
        crocodile1.eat();
        crocodile1.sleep();
        crocodile1.swim();

        Submarine submarine1 = new Submarine("Fighting", "Turbo");
        submarine1.swim();

        Flyable[] flyablesObjects = {bat1,plane1};
        for(int i=0; i<flyablesObjects.length;i++){
            flyablesObjects[i].takeoff();
            flyablesObjects[i].fly();
            flyablesObjects[i].landing();
        }
    }
}
