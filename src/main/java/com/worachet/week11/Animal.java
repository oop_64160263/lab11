package com.worachet.week11;

public abstract class Animal {
    private String name;
    private int numberOfLeg;
    public Animal(String name,int numberOfLeg){
        this.name=name;
        this.numberOfLeg=numberOfLeg;
    }
    public String getName(){
        return name;
    }
    public int getNumberOfLeg(){
        return numberOfLeg;
    }
    public void setName(){
        this.name=name;
    }
    public void setNumberOfLeg(){
        this.numberOfLeg=numberOfLeg;
    }
    @Override
    public String toString() {
        return "Animal ("+name+") had "+ numberOfLeg+ "legs";
    }

    public abstract void eat();
    public abstract void sleep();
}
